package com.safebear.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import pages.UserPage;
import pages.WelcomePage;
import utils.Utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by cca_student on 20/11/2017.
 */
public class BaseTest {

    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp(){

        utility = new Utils();
        driver = utility.getDriver();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);

        driver.get(utility.getUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
     @After
     public void tearDown(){
         try {
             TimeUnit.SECONDS.sleep(2);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         driver.quit();
     }
}
