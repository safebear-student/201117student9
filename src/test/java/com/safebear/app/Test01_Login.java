package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by cca_student on 20/11/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        //Step 1 Confirm we're on the Welcome Page
        assertTrue (welcomePage.checkCorrectPage());

        // Step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));

        //Step 4 Logout
        assertTrue(userPage.clickOnLogout(this.welcomePage));
    }

}
