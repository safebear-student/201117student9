package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by cca_student on 20/11/2017.
 */
public class UserPage {

    WebDriver driver;

    @FindBy(linkText = "Logout")
    WebElement logoutlink;

    public UserPage (WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage() {
        return driver. getTitle().startsWith("Logged In");
    }

    public boolean clickOnLogout(WelcomePage welcomePage)
    {
        logoutlink.click();
        return welcomePage.checkCorrectPage();
    }

}
